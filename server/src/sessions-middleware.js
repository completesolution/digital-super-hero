const pg = require('pg')
const session = require('express-session')
const pgSession = require('connect-pg-simple')(session)

const pgPool = new pg.Pool({
	user: 'superhero',
	host: '77.222.55.151',
	database: 'superhero',
	password: 'RkfhrRtyn',
	port: 5432
})

pgPool.connect((err) => {
	if (err) {
		throw err
	}

	console.log('Connected to PG')
})

module.exports = session({
	store: new pgSession({
		pool: pgPool,
		tableName: 'session'
	}),
	secret: 'Кларк Кент',
	resave: true,
	saveUninitialized: true,
	cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 } // 30 days
})
