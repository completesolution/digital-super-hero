const fs = require('fs')
const path = require('path')
const express = require('express')
const cors = require('cors')
const { ApolloServer, gql } = require('apollo-server-express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const prisma = require('./prisma')
const { graphqlUploadExpress } = require('graphql-upload')

const resolvers = require('./resolvers')

const server = new ApolloServer({
	typeDefs: gql(
		fs
			.readFileSync(
				path.resolve(
					__dirname,
					'schema.graphql'
				),
				'utf8'
			)
	),
	resolvers,
	context: async ({ req }) => {
		const token = req.headers.authorization || ''
		let user

		try {
			const { userId } = jwt.verify(token, 'kryptonite')
			user = await prisma.query.user({
				where: { id: userId }
			})
		} catch(err) {
			user = null
		}

		return {
			prisma,
			jwt,
			bcrypt,
			user
		}
	},
	uploads: true
})

const app = express()
app.use(cors({ credentials: true }))
app.use('/public', express.static('public'))

// app.use(sessionsMiddleware)
// app.use(
// 	'/graphql',
// 	graphqlUploadExpress({})
// )
server.applyMiddleware({
	app
})

app.listen({
   port: 4000
}, () => console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`));
