const uniqueFilename = require('unique-filename')
const fs = require('fs')
const path = require('path')
const moment = require('moment')

const Mutation = {
	async signUp(parent, arg, { prisma, jwt, bcrypt }, info) {
		const { name, email, password, userRole, dealRole, inn} = arg.data

		const userExists = await prisma.exists.User({ email })

		if (userExists) {
			throw new Error('User already exists')
		}

		const user = await prisma.mutation.createUser({
			data: {
				name: name,
				email: email,
				userRole: userRole,
				dealRole: dealRole,
				password: await bcrypt.hash(password, 10),
				property: {
					create: {
						inn: inn
					}
				}
			},
		})

		return {
			token: jwt.sign({ userId: user.id }, 'kryptonite'),
			user
		}
	},

	async signIn(parent, arg, { prisma, jwt, bcrypt }, info) {
		const { email, password } = arg.data

		const userExists = await prisma.exists.User({ email })

		if (!userExists) {
			throw new Error('User not found')
		}

		const user = await prisma.query.user({
			where: { email }
		})

		const passwordValid = await bcrypt.compare(password, user.password)

		if (!passwordValid) {
			throw new Error('Password not valid')
		}

		return {
			token: jwt.sign({ userId: user.id }, 'kryptonite'),
			user
		}
	},
	approveUser: async (parent, args, { prisma }, info) => {
		const { data } = args
		return prisma.mutation.updateUser({
				where: {
							id: data.user,
				},
				data: {
					approval: true
			}
		}, info)
	},

	singleUpload: async (parent, args, ctx, info) => {
		console.log(args)
		const { filename, mimetype, encoding, createReadStream } = await args.file
		const pathName = `${uniqueFilename('../public')}${path.extname(filename)}`

		await new Promise(resolve =>
			createReadStream()
				.pipe(fs.createWriteStream(pathName))
				.on('finish', resolve)
		)
		console.log(filename)

		return {
			filename: pathName,
			mimetype,
			encoding
		}
  },
  uploadFileToCompetition: async (parent, args, { prisma }, info) => {
		 const  data = args.data
		 console.log(data)
		 const dataFile = await Mutation.singleUpload(parent, data, { prisma }, info)
		 const competition = await prisma.query.competition({
 						where: {
 								id: data.competition
 						}
 				}, info)
		 const file = await prisma.mutation.createFile({
		 data: {
			 filename: dataFile.filename,
			 mimetype: dataFile.mimetype,
			 encoding: dataFile.encoding,
			 competition: {
				 connect: {
					    id: data.competition
						}
					},
			 owner: {
				 connect: {
							id: data.owner
						}
				 },
			 type: data.type
		   }
	   })
		 return competition
	},
	createCompetition: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.mutation.createCompetition({
			data: {
				...data,
				owner: {
					connect: {
						id: data.owner
					}
				},
				job: { create: {} }
			}
		})

		return competition
	},

	setChampion: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		const champion = await prisma.query.user({
            where: {
                id: data.champion
            }
        })
		return prisma.mutation.updateJob({
				where: {
						id: competition.job.id
				},
				data: {
					champion: {
						connect: {
							id: champion.id
						}
					},
					startTime: moment(Date.now()).format()
				}
		}, info)
	},

	createBet: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
					}, '{ id owner { id } }')
		const participant = await prisma.query.user({
						where: {
								id: data.participant
						}
				})
		const bet = await prisma.mutation.createBet({
			data: {
				competition:  {
					connect: {
						id: data.competition
					}
				},
				participant:  {
					connect: {
						id: data.participant
					}
				},
				value: data.value,
				room: {
					create: {}
				}
			}
		})
		return bet
	},

	approveByCustomerProjectTime: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					approvalByCustomerProjectTime: moment(Date.now()).format()
				}
		}, info)
	},

	approveByGridcomProjectTime: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					approvalByGridcomProjectTime: moment(new Date()).format()
				}
		}, info)
	},

	approveFinishWorkTime: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					finishWorkTime: moment(new Date()).format()
				}
		}, info)
	},

	approveByCustomerFinishTime: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					approvalByCustomerFinishTime: moment(new Date()).format()
				}
		}, info)
	},

	approveByGridcomFinishTime: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					approvalByGridcomFinishTime: moment(new Date()).format()
				}
		}, info)
	},

	setRate: async (parent, args, { prisma }, info) => {
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					rate: data.rate
			}
		}, info)
	},

	declineByCustomerProjectTime: async (parent, args, { prisma }, info) => {
		// added methods for files
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					approvalByCustomerProjectTime: null
				}
		}, info)
	},

	declineByGridcomProjectTime: async (parent, args, { prisma }, info) => {
		// added methods for files
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					approvalByCustomerProjectTime: null,
					approvalByGridcomProjectTime: null
				}
		}, info)
	},

	declineByCustomerFinishTime: async (parent, args, { prisma }, info) => {
		// added methods for files
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					finishWorkTime: null,
					approvalByCustomerFinishTime: null,
					approvalByGridcomFinishTime: null
				}
		}, info)
	},

	declineByGridcomFinishTime: async (parent, args, { prisma }, info) => {
		// added methods for files
		const { data } = args
		const competition = await prisma.query.competition({
						where: {
								id: data.competition
						}
				}, '{ id job { id } }')
		return prisma.mutation.updateJob({
				where: {
							id: competition.job.id,
				},
				data: {
					finishWorkTime: null,
					approvalByCustomerFinishTime: null,
					approvalByGridcomFinishTime: null
				}
		}, info)
	},
	createComment: async (parent, args, { prisma }, info) =>
	{
		const { data } = args

		return prisma.mutation.createComment(
			{
				data: {
					author: {
						connect: {
							id: data.userId
						}
					},
					message: data.message,
					room: {
						connect: {
							id: data.roomId
						}
					},
					date: moment(new Date()).format()
					}
			})}
}

module.exports = Mutation
