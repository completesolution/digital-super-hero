const Query = {
   users(parent, args, { prisma }, info) {
      return prisma.query.users(null, info)
   },
   me(parent, args, { prisma, user }, info) {
      if (user) {
         return user
      }

      throw new Error('You are not signed in')
   },
   user(parent, args, { prisma }, info) {
      return prisma.query.user({
         where: {
            id: args.userId
         }
      }, info)
   },
   competition(parent, args, { prisma }, info) {
      return prisma.query.competition({
         where: {
            id: args.competitionId
         }
      }, info)
   },
   competitions(parent, args, { prisma }, info) {
     const opArgs = {
        first: args.first,
        skip: args.skip,
        after: args.after,
        orderBy: args.orderBy
     }
     if (parent) {
            opArgs.where = {
                OR: [{
                    owner: parent.id
                }]
            }
          }
     return prisma.query.competitions(opArgs, info)
   },
   jobs(parent, args, { prisma }, info) {
     const opArgs = {
         first: args.first,
         skip: args.skip,
         after: args.after,
         orderBy: args.orderBy
     }
     if (parent) {
            opArgs.where = {
                OR: [{
                    champion: parent.id
                }]
            }
          }
     return prisma.query.jobs(opArgs, info)
   },
   room(parent, args, { prisma }, info) {
     return prisma.query.room({
        where: {
           id: args.roomId
        }
     }, info)
   },
   bet(parent, args, { prisma }, info) {
       return prisma.query.bet({
          where: {
             id: args.betId
          }
       }, info)
     },
   myBet(parent, args, { prisma }, info) {
     console.log(args)
     return prisma.query.bets.filter({value: 1000
      })
   },
   bets(parent, args, { prisma }, info) {
     const opArgs = {
         first: args.first,
         skip: args.skip,
         after: args.after,
         orderBy: args.orderBy
     }
     if (parent) {
            opArgs.where = {
                OR: [{
                    competition: {
                      id: parent.id
                    }
                }]
            }
          }
     return prisma.query.bets(opArgs, info)
   },
   rooms(parent, args, { prisma }, info) {
     const opArgs = {
         first: args.first,
         skip: args.skip,
         after: args.after,
         orderBy: args.orderBy
     }
     if (parent) {
            opArgs.where = {
                OR: [{
                    owner: parent.id
                }, {
                    participant: parent.id
                }]
            }
          }
     return prisma.query.rooms(opArgs, info)
   },
		comments(parent, args, { prisma }, info){
      const opArgs = {
          first: args.first,
          skip: args.skip,
          after: args.after,
          orderBy: args.orderBy
      }
      if (parent) {
             opArgs.where = {
                 OR: [{
                     room: parent.id
                 }]
             }
           }
      return prisma.query.comment(opArgs, info)
		},
		fileOffer(parent, args, { prisma }, info){
			return prisma.query.comment({
				where: {
					participant: args.userId,
					competition: args.competitionId
				}
			}, info)
		},

}

module.exports = Query
