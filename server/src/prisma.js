const { Prisma } = require('prisma-binding')

const prisma = new Prisma({
	typeDefs: 'prisma/generated/prisma.graphql',
	endpoint: 'http://77.222.55.151:4466/default',
	secret: 'my-super-secret-secret'
})

module.exports = prisma
