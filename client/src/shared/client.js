import ApolloClient from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { createUploadLink } from 'apollo-upload-client'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import mutex from 'link-mutex'

const httpLink = createUploadLink({
 	uri: 'http://localhost:4000/graphql'
})

const authLink = setContext(async (_, { headers }) => {
	await mutex.freed

   const token = localStorage.getItem('token')

   return {
      headers: {
         ...headers,
         authorization: token || ''
      }
   }
})

const client = new ApolloClient({
	link: authLink.concat(httpLink),
	cache: new InMemoryCache(),
	defaultOptions: {
		watchQuery: {
			errorPolicy: 'ignore'
		},
		query: {
			errorPolicy: 'all'
		}
	}
})

export default client
