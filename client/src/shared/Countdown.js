import React from 'react'
import { Statistic, Icon } from 'antd'
import { statisticLabel, statisticValue } from 'cs'

const { Countdown } = Statistic

const defineFormat = (time, valueStyle) => {
	const diff = (time - Date.now()) / 1000 / 60 / 60 / 24
	if (diff > 1) {
		return {
			format: 'D',
			suffix: pug`span(style=statisticLabel(valueStyle)) д.`
		}
	}
	
	return {
		format: 'HH:mm:ss',
		suffix: ''
	}
}

export default ({ id, time, titleStyle, valueStyle }) => {
	
	
	return pug`
		Countdown(
			prefix=${pug`Icon(type='clock-circle')`}
			title=${pug`
				span(
					style=statisticLabel(titleStyle)
				) До завершения
			`}
			format=defineFormat(time).format
			value=${time}
			valueStyle=statisticValue(valueStyle)
			suffix=defineFormat(time, valueStyle).suffix
		)
	`
}
