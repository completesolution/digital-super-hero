export default {
	freed: Promise.resolve(),

	lock() {
		const mutex = this

		mutex.freed = new Promise(resolve => {
			mutex.unlock = resolve
		})
	}
}
