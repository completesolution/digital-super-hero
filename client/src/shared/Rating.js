import React from 'react'

import { Rate } from 'antd'

export default ({ rating }) => {
	const roundedRating = Math.round(rating * 2) / 2
	return pug`
		Rate(value=roundedRating disabled allowHalf)
	`
}
