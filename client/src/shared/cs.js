const custom = preset => custom => ({
	...preset,
	...custom
})

export const layoutPadding = custom({
	padding: '25px'
})

export const defaultListItemStyle = custom({
	'border': '1px solid #e8e8e8',
	'margin': '10px',
	'padding': '10px',
	'background': 'white'
})

export const statisticValue = custom({
	'textAlign': 'center',
	'fontSize': '14px'
}) 

export const statisticLabel = custom({
	'display': 'block',
	'width': '100%',
	'textAlign': 'center',
	'fontSize': '12px'
})

export const sidebar = custom({
	'background': 'white',
	'paddingBottom': '10px'
})

export const tabs = custom({
	'background': 'white',
	'padding': '20px'
})

export const competition = custom({
	'paddingLeft': '40px',
	'paddingRight': '40px'
})

export const customerInfo = custom({
	'marginBottom': '20px',
	'textAlign': 'center'
})
export const customerInfoTitle = custom({
	'fontWeight': 'bold'
})

export const customerInfoBody = custom({
})

export const offerRows = custom({
	'marginBottom': '20px'
})

export const cardHead= custom({
	'fontSize': '16px',
	'textAlign': 'center'
})