import React from 'react'
import { Field } from 'react-final-form'
import { Rate, Form } from 'antd'

const FormItem = Form.Item

const RateAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest,
}) => pug`
		FormItem(
			layout=layout
			wrapperCol=wrapperCol
			labelCol=labelCol
			validateStatus=meta.invalid ? 'error' : null
			help=meta.error
		)
			Rate(
				...rest
				onChange = value => input.onChange(value)
			)
	`

export default class RateComponent extends React.Component {
	render() {
		const { props } = this
		
		return pug`
			Field(
				...props
				component=RateAdapter
			)
		`
	}
}
