import React from 'react'
import { Field } from 'react-final-form'
import { TimePicker, Form } from 'antd'

const FormItem = Form.Item

const TimePickerAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest,
}) => pug`
		FormItem(
			layout=layout
			wrapperCol=wrapperCol
			labelCol=labelCol
			validateStatus=meta.invalid ? 'error' : null
			help=meta.error
		)
			TimePicker(
				...rest
				onChange = value => input.onChange(value.format())
			)
	`

export default class TimePickerComponent extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=TimePickerAdapter
			)
		`
	}
}
