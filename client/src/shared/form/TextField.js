import React from 'react'
import { Field } from 'react-final-form'
import { Input, Form } from 'antd'

const FormItem = Form.Item

const TextFieldAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest
}) => pug`
	FormItem(
		layout=layout
		wrapperCol=wrapperCol
		labelCol=labelCol
		validateStatus=meta.error || meta.submitError ? 'error' : null
		help=meta.error || meta.submitError
	)
		Input(
			...input
			...rest
			onChange=(value)=>input.onChange(value)
		)
`

export default class TextField extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=TextFieldAdapter
			)
		`
	}
}
