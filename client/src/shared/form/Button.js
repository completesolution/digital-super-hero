import React from 'react'
import { Field } from 'react-final-form'
import { Button } from 'antd'

const ButtonAdapter = ({
	options,
	meta,
	label,
	...rest
}) => {
	return pug`
	Button(
		...rest
		onClick=rest.onClick
		type="primary"
	) ${label}
`}


export default class FunctionalButton extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=ButtonAdapter
			)
		`
	}
}
