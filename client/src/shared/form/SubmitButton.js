import React from 'react'
import { Field } from 'react-final-form'
import { Button } from 'antd'

const SubmitButtonAdapter = value => ({
	options,
	meta,
	...rest
}) => pug`
	Button(
		type="primary"
		htmlType="submit"
	) ${value || 'Отправить'}
`


export default class SubmitButton extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=SubmitButtonAdapter(props.value)
			)
		`
	}
}
