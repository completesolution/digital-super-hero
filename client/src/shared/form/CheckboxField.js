import React from 'react'
import { Field } from 'react-final-form'
import { Checkbox, Form } from 'antd'
const FormItem = Form.Item

const CheckboxGroup = Checkbox.Group

const CheckboxAdapter = ({
	options,
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest
}) => pug`
	FormItem(
		layout=layout
		wrapperCol=wrapperCol
		labelCol=labelCol
		validateStatus=meta.error ? 'error' : null
		help=meta.error
	)
		CheckboxGroup(
			...rest
			options=options,
			onChange = value => input.onChange(value)
		)
`


export default class CheckboxComponent extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=CheckboxAdapter
			)
		`
	}
}
