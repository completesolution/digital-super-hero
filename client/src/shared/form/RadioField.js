import React from 'react'
import { Field} from 'react-final-form'
import { Radio, Form  } from 'antd'
const FormItem = Form.Item
const RadioGroup = Radio.Group;

const RadioFieldAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest
}) => pug`
	FormItem(
		layout=layout
		wrapperCol=wrapperCol
		labelCol=labelCol
		validateStatus=meta.error ? 'error' : null
		help=meta.error
	)
		RadioGroup(
			...rest
			onChange=value => {
				input.onChange(value)
				if (rest.onChange) rest.onChange(value)
			}
		)
`


export default class RadioField extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				type='radio'
				component=RadioFieldAdapter
			)
		`
	}
}
