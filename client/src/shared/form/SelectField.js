import React from 'react'
import { Field } from 'react-final-form'
import { Select, Form } from 'antd';

const FormItem = Form.Item
const Option = Select.Option
const SelectAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest,
}) => pug`
	FormItem(
		layout=layout
		wrapperCol=wrapperCol
		labelCol=labelCol
		validateStatus=meta.invalid ? 'error' : null
		help=meta.error
	)
		Select(
			...rest
			optionFilterProp='children'
			onChange = value => input.onChange(value)
		)
`

export default class SelectField extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props,
				component=SelectAdapter
			)
		`
	}
}
