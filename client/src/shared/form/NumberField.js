import React from 'react'
import { Field } from 'react-final-form'
import { InputNumber, Form } from 'antd'
const FormItem = Form.Item

const InputNumberAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest
}) => pug`
	FormItem(
		layout=layout
		wrapperCol=wrapperCol
		labelCol=labelCol
		validateStatus=meta.invalid ? 'error' : null
		help=meta.error
	)
		InputNumber(
			...rest
			name=input.name
			onChange=${ (number) => input.onChange(number) }
		)
`


export default class InputNumberComponent extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=InputNumberAdapter
			)
		`
	}
}
