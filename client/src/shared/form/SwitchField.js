import React from 'react'
import { Field } from 'react-final-form'
import { Switch, Form } from 'antd'

const FormItem = Form.Item

const SwitchAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest,
}) => pug`
		FormItem(
			layout=layout
			wrapperCol=wrapperCol
			labelCol=labelCol
			validateStatus=meta.invalid ? 'error' : null
			help=meta.error
		)
			Switch(
				...rest
				onChange = value => input.onChange(value)
			)
	`

export default class SwitchComponent extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=SwitchAdapter
			)
		`
	}
}
