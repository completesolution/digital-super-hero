import React from 'react'
import { Field } from 'react-final-form'
import { Input, Form } from 'antd'
const FormItem = Form.Item
const TextArea = Input.TextArea

const TextAreaFieldAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest
}) => pug`
	FormItem(
		layout=layout
		wrapperCol=wrapperCol
		labelCol=labelCol
		validateStatus=meta.invalid ? 'error' : null
		help=meta.error
	)
		TextArea(
			...input
			...rest
			onChange=(value)=>input.onChange(value)
		)
`

export default class TextAreaField extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=TextAreaFieldAdapter
			)
		`
	}
}
