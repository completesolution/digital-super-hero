import React from 'react'
import { Field } from 'react-final-form'
import {
	Upload, message, Button, Icon
} from 'antd'

const props = {
	name: 'file',
	action: '//jsonplaceholder.typicode.com/posts/',
	headers: {
		authorization: 'authorization-text',
	},
	// onChange(info) {
	// 	if (info.file.status !== 'uploading') {
	// 		console.log(info.file, info.fileList);
	// 	}
	// 	if (info.file.status === 'done') {
	// 		message.success(`${info.file.name} file uploaded successfully`);
	// 	} else if (info.file.status === 'error') {
	// 		message.error(`${info.file.name} file upload failed.`);
	// 	}
	// },
}

const UploadAdapter = ({
	meta,
	input,
	...rest
}) => pug`
		Upload(
			...rest
			...props
			onChange = value => input.onChange(value)
		)
			Button
				Icon(type='upload')
	`
export default class UploadComponent extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=UploadAdapter
			)
		`
	}
}
