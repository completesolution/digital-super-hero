import React from 'react'
import { Form as FormFinal } from 'react-final-form'
import { Form as FormAnt } from 'antd'


export default class Form extends React.Component {
	constructor(props) {
		super(props)

		this.form = React.createRef()
	}

	render() {
		const { children, render, onSubmit, validate, ...rest } = this.props

		return pug`
			FormFinal(
				...rest
				onSubmit=onSubmit
				validate=validate
				ref=this.form
				render=${
					({ handleSubmit }) => pug`
						FormAnt(
							...rest
							onSubmit=handleSubmit
						)
							${children}
					`
				}
			)
		`
	}
}

// export default ({ children, render, onSubmit, validate, ...rest }) => {
// 	return pug`
// 		FormFinal(
// 			...rest
// 			onSubmit=onSubmit
// 			validate=validate
// 			render=${
// 				({ handleSubmit }) => pug`
// 					FormAnt(
// 						...rest
// 						onSubmit=handleSubmit
// 					)
// 						${children}
// 				`
// 			}
// 		)
// 	`
// }
