import React from 'react'
import { Field } from 'react-final-form'
import { DatePicker, Form } from 'antd'

const FormItem = Form.Item

const DatePickerAdapter = ({
	input,
	meta,
	layout,
	wrapperCol,
	labelCol,
	...rest,
}) => pug`
		FormItem(
			layout=layout
			wrapperCol=wrapperCol
			labelCol=labelCol
			validateStatus=meta.invalid ? 'error' : null
			help=meta.error
		)
			DatePicker(
				...rest
				onChange = value => input.onChange(value.format())
			)
	`

export default class DatePickerComponent extends React.Component {
	render() {
		const { props } = this
		return pug`
			Field(
				...props
				component=DatePickerAdapter
			)
		`
	}
}
