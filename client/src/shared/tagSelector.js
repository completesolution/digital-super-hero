import { Tag } from 'antd'
import React from 'react'

export const tagByType = type => {
	switch(type) {
		case 'TENDER': return pug`Tag(color="green") Тендер`
		case 'AUCTION': return pug`Tag(color="volcano") Аукцион`
		case 'COMPETITION': return pug`Tag(color="blue") Конкурс`
		default: return pug`Tag(color="gray") Неизвестно`
	}
}
