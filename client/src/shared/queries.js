import gql from 'graphql-tag'

export const getUsers = gql`
	query {
		users {
			id
			name
			email
			userRole
			dealRole
		}
	}
`

export const getCompetitions = gql`
	query {
		competitions {
			id
			title
			description
			typeCompetition
			timeLimit
			maxValue
		}
	}
`

export const signIn = gql`
	mutation SignIn($email: String!, $password: String!){
		signIn(
			data: {
				email: $email
				password: $password
			}
		) {
			token
			user {
				id
				name
				email
				userRole
				dealRole
			}
		}
	}
`

export const createCompetition = gql`
	mutation CreateCompetition(
		$owner: ID!
		$title: String!,
		$description: String!,
		$publishedAt: Date!,
		$timeLimit: Date!,
		$maxValue: Int!,
		$typeCompetition: TypeCompetition!
	){
		createCompetition(
			data: {
				owner: $owner
				title: $title
				description: $description
				publishedAt: $publishedAt
				timeLimit: $timeLimit
				maxValue: $maxValue
				typeCompetition: $typeCompetition
			}
		) {
			id
			title
		}
	}
`

export const signUp = gql`
	mutation SignUp(
		$name: String!,
		$email: String!,
		$password: String!,
		$userRole: UserRole!,
		$dealRole: DealRole!
	){
		signUp(
			data: {
				name: $name
				email: $email
				password: $password
				userRole: $userRole
				dealRole: $dealRole
			}
		) {
			token
			user {
				id
				name
				email
				userRole
				dealRole
			}
		}
	}
`

export const me = gql`
	query {
		me {
			id
			name
			email
			userRole
			dealRole
		}
	}
`
export const uploadFiles = gql`
	mutation SingleUpload($file: Upload!) {
		singleUpload(file: $file) {
			filename
			mimetype
			encoding
		}
	}
`
