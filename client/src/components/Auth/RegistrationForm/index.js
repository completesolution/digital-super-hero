import React, { Component } from 'react';
import { Modal, Radio, Button, Spin, Icon } from 'antd';
import { withRouter, Link } from 'react-router-dom'
import {
	Form,
	TextField,
	RadioField
} from 'form'

import mutex from 'link-mutex'
import { graphql } from 'react-apollo'
import { signUp, me } from 'queries'

const RadioButton = Radio.Button

class RegistrationForm extends Component {
	form = React.createRef()
	modal = React.createRef()
	state = { subject: 'none', loading: false }

	changeRole = (e) => {
		this.setState({
			subject: e.target.value,
		})
	}

	emitSubmit = () => {
		this.form.current.form.current.handleSubmit()
	}

	handleSubmit = async values => {
		values.userRole = values.userRole || 'BUILDER'

		const errors = {}
		const { modal } = this

		this.setState({ loading: true })

		try {
			const {
				data: { signUp: { token } }
			} = await this.props.mutate({
				variables: values,
				refetchQueries: () => {
					mutex.lock()
					return [{
						query: me
					}]
				}
			})

			localStorage.setItem('token', token)
			mutex.unlock()
			modal.current.handleCancel()
		} catch (err) {
			errors.email = 'Этот e-mail уже зарегистрирован'
			this.setState({ loading: false })
		}

		return errors
	}

	render() {
		const { modal, form, emitSubmit } = this
		const { subject, loading } = this.state

		const isEntity = subject === 'ENTITY'
		const isIndividual = subject === 'INDIVIDUAL'

		return pug`
			Modal(
				title='Форма Регистрации'
				visible=this.props.visible
				onCancel=this.props.handleCancel
				ref=modal
				footer=${
					pug`
						Button(
							key='enter'
							type='primary'
							onClick=emitSubmit
							loading=loading
						)
							| Зарегистрироваться
					`
				}
			)
				Spin(
					spinning=loading
					indicator=${pug`Icon(type='loading')`}
				)
					Form(ref=form onSubmit=this.handleSubmit)
						RadioField(name='dealRole' onChange=this.changeRole defaultValue='ENTITY')
							RadioButton(value='ENTITY') Юридическое лицо
							RadioButton(value='INDIVIDUAL') Физическое лицо
						TextField(
							placeholder='Введите имя...'
							name='name'
						)
						TextField(
							placeholder='Введите email...'
							name='email'
						)
						TextField(
							placeholder='Введите пароль...'
							name='password'
							type='password'
						)
						RadioField(name='userRole')
							RadioButton(value='CUSTOMER' disabled=isIndividual) Заказчик
							RadioButton(value='BUILDER' checked=isIndividual) Подрядчик
		`
	}
}

export default withRouter(graphql(signUp)(RegistrationForm))
