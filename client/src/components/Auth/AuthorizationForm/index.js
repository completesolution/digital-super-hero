import React, { Component } from 'react';
import { Modal, Spin, Icon, Button } from 'antd';
import { withRouter, Link } from 'react-router-dom'
import {
	Form,
	TextField,
	PasswordField
} from 'form'

import { graphql } from 'react-apollo'
import { signIn, me } from 'queries'
import { get } from 'lodash/fp'
import mutex from 'link-mutex'

const sleep = delay => new Promise(resolve => setTimeout(resolve, delay))

const getToken = get(['signIn', 'token'])
const getUser = get(['signIn', 'user'])

class AuthorizationForm extends Component {
	form = React.createRef()
	modal = React.createRef()
	state = { loading: false }

	emitSubmit = () => {
		this.form.current.form.current.handleSubmit()
	}

	handleSubmit = async (values) => {
		const errors = {}
		const { modal } = this

		this.setState({ loading: true })

		try {
			const {
				data: { signIn: { token } }
			} = await this.props.mutate({
				variables: values,
				refetchQueries: () => {
					mutex.lock()
					return [{
						query: me
					}]
				}
			})

			localStorage.setItem('token', token)
			mutex.unlock()
			modal.current.handleCancel()
		} catch (err) {
			errors.email = ' '
			errors.password = 'Неверный E-mail или пароль'
			this.setState({ loading: false })
		}

		return errors
	}

	render() {
		const { loading } = this.state
		const { props, emitSubmit, form, handleSubmit, modal } = this

		return pug`
			Modal(
				ref=modal
				title='Форма Авторизации'
				visible=props.visible
				onCancel=props.handleCancel
				footer=${
					pug`
						Button(
							key='enter'
							onClick=emitSubmit
							loading=loading
							type='primary'
						) Войти`
				}
			)
				Spin(
					spinning=loading
					indicator=${pug`Icon(type='loading')`}
				)
					Form(
						ref=form
						onSubmit=handleSubmit
					)
						TextField(
							placeholder='Введите email...'
							name='email'
						)
						TextField(
							placeholder='Введите пароль...'
							name='password'
							type='password'
						)
		`
	}
}

export default withRouter(graphql(signIn)(AuthorizationForm));
