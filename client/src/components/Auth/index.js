import React, { Component } from 'react';
import { Modal, Icon, Typography, Col, Button, Avatar } from 'antd'
import { withRouter, Link } from 'react-router-dom'
import AuthorizationForm from './AuthorizationForm'
import RegistrationForm from './RegistrationForm'
import {
	Form
} from 'form'
import {
	Form as AntForm
} from 'antd'

import { graphql } from 'react-apollo'
import { me } from 'queries'
import { get } from 'lodash/fp'
import client from 'client'

const getName = get(['name'])

const { Text } = Typography
const { Item } = AntForm

class Test extends Component {
	state = {
		visibleAuthorizationForm: false,
		visibleRegistrationForm: false
	}

	showAuthorizationForm = () => {
		this.setState({
			visibleAuthorizationForm: true,
		})
	}

	showRegistrationForm = () => {
		this.setState({
			visibleRegistrationForm: true,
		})
	}

	handleCancel = () => {
		this.setState({
			visibleAuthorizationForm: false,
			visibleRegistrationForm: false
		})
	}

	handleLogOut = () => {
		localStorage.setItem('token', '')
		client.resetStore()
	}

	render() {
		const { authorized, userName, data: { loading, me } } = this.props
		const {
			handleLogOut,
			state,
			handleCancel,
			showAuthorizationForm,
			showRegistrationForm
		} = this

		console.log(me);

		return pug`
			Form(layout='inline' onSubmit=${() => {}})
				if !me
					Item
						Button(type='primary', onClick=showAuthorizationForm ghost) Вход
					Item
						Button(type='primary', onClick=showRegistrationForm) Регистрация
					AuthorizationForm(
						visible=state.visibleAuthorizationForm
						handleCancel=handleCancel
					)
					RegistrationForm(
						visible=state.visibleRegistrationForm
						handleCancel=handleCancel
					)
				else
					if me.userRole == 'CUSTOMER'
						Item
							Link(to=${`/competition/new`})
								Button(type='primary') Создать конкурс
					Item
						Avatar(size='large' icon='user' style=${{ backgroundColor: '#7265e6' }})
					Item
						h3 ${getName(me)}
					Item
						Button(type='primary' onClick=handleLogOut ghost) Выход
		`
	}
}

export default withRouter(graphql(me)(Test));
