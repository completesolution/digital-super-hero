import React, { Component } from 'react';
import Test from './scenes/Test'
import Main from './scenes/Main'
import Competition from './scenes/Competition'
import CreateCompetition from './scenes/CreateCompetition'
import SignUp from './scenes/SignUp'
import Profile from './scenes/Profile'
import Auth from './components/Auth'
import { BrowserRouter } from 'react-router-dom'
import { Route, Switch } from 'react-router'
import { Grid, PageHeader, Layout } from 'antd'
import './style.css'
import { layoutPadding } from 'cs'

const routes = [
   {
      path: 'index',
      breadcrumbName: 'First-level Menu'
   }, {
      path: 'first',
      breadcrumbName: 'Second-level Menu'
   }, {
      path: 'second',
      breadcrumbName: 'Third-level Menu'
   }
]

const { Header, Content } = Layout
class App extends Component {
	render() {
		return pug`
			BrowserRouter
				Layout(style={minHeight: '100vh'})
					PageHeader(
						title='Конкурсы'
						extra=${[
							pug`Auth(key=0)`
						]}
						breadcrumb=${{ routes }}
					)
					Content(style=layoutPadding())
						Switch
							Route(exact path='/' render=Main)
							Route(path='/competition/new' render=CreateCompetition)
							Route(path='/competition/:id' render=Competition)
							Route(path='/sign-up' render=SignUp)
							Route(path='/profile/:id' render=Profile)
		`
	}
}

export default App;
