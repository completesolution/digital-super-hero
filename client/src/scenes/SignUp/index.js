import { withRouter } from 'react-router-dom'
import React from 'react'
import { get } from 'lodash/fp'
import { graphql } from 'react-apollo'

const getName = get(['name'])

import { Query } from 'react-apollo'
import { me } from 'queries'

const SignUp = props => {
	const { loading, error, me } = props.data

	return pug`
		span Sign
		br
		if loading
			span Loading...
		if !(loading || me)
			span Error
		if me
			span ${getName(me)}
	`
}

export default withRouter(graphql(me)(SignUp))
