import React, { Component } from 'react';
import { Radio, Select } from 'antd';
import { withRouter, Link } from 'react-router-dom'
import {
	Form,
	CheckboxField,
	NumberField,
	RadioField,
	TextField,
	TextAreaField,
	SubmitButton,
	SelectField,
	DateField,
	SwitchField,
	RateField,
	TimeField,
} from 'form'

const Option = Select.Option

const onSubmit = values => {
	console.log(values)
}
const options = [ 'Apple', 'Kek', 'Stas' ]
const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 5 },
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 12 },
	},
};

const validate = (values) => {
	const errors = {}
	
	if (values['stas-text'] && values['stas-text'].length > 3) {
		errors['stas-text'] = 'Длинный текст'
	}

	if (values['stas-date-picker'] && values['stas-date-picker'].substring(0, 4) !== '2019') {
		errors['stas-date-picker'] = 'Текущий год'
	}

	if (values['stas-select'] && values['stas-select'] !== 'stas') {
		errors['stas-select'] = 'Выбран не стас'
	}

	if (values['stas-switch']) {
		errors['stas-switch'] = 'Выключи'
	}

	if (values['stas-rate'] && values['stas-rate'] < 4) {
		errors['stas-rate'] = 'Не стоит недооценивать мою мощь'
	}

	if (values['stas-time-picker'] && values['stas-time-picker'].indexOf('20:') === -1) {
		errors['stas-time-picker'] = 'Переведи часы на 8 вечера аллло'
	}

	if (!values['stas-number']) {
		errors['stas-number'] = 'Need number'
	}
	errors['stas-checkbox'] = 'STAS'
	errors['stas-text-area'] = 'STAS'
	errors['stas-radio'] = 'STAS'
	return errors
}

class Test extends Component {
	render() {
		return pug`
			Form(
				onSubmit=onSubmit
				validate=validate
			)
				TextField(
					...formItemLayout
					placeholder='Oleg'
					name='stas-text'
				)
				TextAreaField(
					...formItemLayout
					placeholder='Oleg'
					name='stas-text-area'
				)
				NumberField(
					name='stas-number'
				)
				RadioField(name='stas-radio')
					Radio(value='A') A
					Radio(value='B') B
					Radio(value='C') C
				CheckboxField(
					name='stas-checkbox'
					options=options
				)
				SelectField(
					...formItemLayout
					name='stas-select'
				)
					Option(key='kek' value='kek') kek
					Option(key='stas' value='stas') stas
					Option(key='osas' value='osas') osas
				DateField(
					...formItemLayout
					name='stas-date-picker'
				)
				SwitchField(name='stas-switch')
				RateField(name='stas-rate' allowHalf)
				TimeField(name='stas-time-picker')
				//- Upload(name='stas-upload')
				SubmitButton
		`
	}
}

export default withRouter(Test);
