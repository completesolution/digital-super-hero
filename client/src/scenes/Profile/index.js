import { withRouter } from 'react-router-dom'
import React from 'react';

const Profile = props => {
		const { match: { params: { id } } } = props
	
	return pug`
		div Profile ${id}
	`
}

export default withRouter(Profile)