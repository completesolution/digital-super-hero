import React from 'react'
import { withRouter } from 'react-router-dom'
import { Route } from 'react-router'
import { Row, Col, Collapse } from 'antd'

import Tabs from './components/Tabs/index'
import CompetitionInfo from './components/CompetitionInfo'
import CustomerInfo from './components/CustomerInfo'
// import Offer from './components/Offer'
import { sidebar, tabs, competition } from 'cs'

class Competition extends React.Component {
	constructor(props) {
		super(props)
		
		this.onChangeTab = this.onChangeTab.bind(this)
	}
	
	componentDidMount() {
		const { url } = this.props.match
		
		this.onChangeTab(`${url}/review`)
	}
	
	onChangeTab(key) {
		this.props.history.push(key)
	}
	
	render() {
		const { match: { params: { id } } } = this.props
		
		return pug`
			Row(style=competition())
				Col(span=17 style=tabs())
					Tabs(onChangeTab=this.onChangeTab)
				Col(offset=1 span=6)
					Row
						CompetitionInfo(id=id)
					Row
						CustomerInfo(id=id)
		`
	}
}

export default withRouter(Competition)