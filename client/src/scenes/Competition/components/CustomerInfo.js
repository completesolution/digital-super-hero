import React from 'react'
import { Row, Rate, Card, Col, Typography, Collapse } from 'antd'
import {
	customerInfo,
	customerInfoTitle,
	customerInfoBody,
	cardHead
} from 'cs'

const { Text } = Typography

const { Panel } = Collapse

export default props => {
	return pug`
		Card(
			title='Информация о заказчике'
			headStyle=cardHead()
			bordered=false
		)
			Row(style=customerInfo())
				div(style=customerInfoTitle()) Название:
				div(style=customerInfoBody()) Какое то название
			Row(style=customerInfo())
				div(style=customerInfoTitle()) ИНН:
				div(style=customerInfoBody()) Какой то ИНН
			Row(style=customerInfo())
					div(style=customerInfoTitle()) Рейтинг:
					Rate(value=1.5 disabled allowHalf)
	`
}