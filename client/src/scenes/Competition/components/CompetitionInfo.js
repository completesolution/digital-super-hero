import React from 'react'
import { withRouter } from 'react-router-dom'
import { Statistic, Icon, Card, Collapse } from 'antd'
import { cardHead } from 'cs'
import Countdown from 'Countdown'

import {
	statisticLabel,
	statisticValue
} from 'cs'

const style = {
	statisticLabel: { fontSize: '16px' },
	statsticValue: { fontSize: '18px', marginBottom: '20px'}
}

const { Panel } = Collapse

export default props => {
	return pug`
		Card(
			title='Информация о конкурсе'
			headStyle=cardHead()
			bodyStyle={ paddingBottom: 0 }
			bordered=false
		)
			Statistic(
				title=${pug`
					span(style=statisticLabel(style.statisticLabel)) Стоимость
				`}
				value='10000000'
				suffix='₽'
				valueStyle=statisticValue(style.statsticValue)
			)
			Statistic(
				title=${pug`
					span(
						style=statisticLabel(style.statisticLabel)
					) Участники`
				}
				value='150'
				prefix=${pug`Icon(type='team')`}
				valueStyle=statisticValue(style.statsticValue)
			)
			Countdown(
				time=${Date.now() + 1000 * 60 * 60 * 24 * 2}
				titleStyle=statisticLabel(style.statisticLabel)
				valueStyle=statisticLabel({ fontSize: '18px' })
			)
	`
}