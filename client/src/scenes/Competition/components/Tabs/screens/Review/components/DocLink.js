import React from 'react'
import { Icon, Row } from 'antd'

export default props => {
	const { doc: { name, link } } = props
	
	return pug`
		Row(style={ marginBottom: '5px' })
			a(href=${`${link}`})
				Icon(type='paper-clip' style={ marginRight: '5px'})
				|Скачать ${name}
	`
}