import React from 'react'
import { withRouter } from 'react-router-dom'
import { PageHeader, Button, Row, Col } from 'antd'

import Description from './components/Description'
import DocLink from './components/DocLink'
import { tagByType } from 'tagSelector' //shared

const dockLinks = [
	{
		name: 'Документ 1',
		link: '#азаза'
	},
	{
		name: 'Документ 2',
		link: '#азаза'
	},
	{
		name: 'Документ 3',
		link: '#азаза'
	}
]

const renderDocLinks = docs => {
	return docs.map((doc, index) => pug`
		DocLink(key=index doc=doc)
	`)
}

class Review extends React.Component {
	constructor(props) {
		super(props)
	}
	
	//вытаскивать данные о competiotion из бд
	render() {
		const { id } = this.props
		
		return pug `
			PageHeader(
				title=${`${id} Title`}
			)
				Row
					${tagByType()}
				Row(style={marginTop: '10px'})
					Description
				Row(style={marginTop: '20px'})
					${renderDocLinks(dockLinks)}
		`
	}
}

export default withRouter(Review)