import { withRouter } from 'react-router-dom'
import React from 'react'
import InfoRow from './InfoRow'
import Rating from 'Rating'

import {
	Row, Col,
	Typography, Divider,
	Statistic, Button, Anchor
} from 'antd'

import {
	defaultListItemStyle,
	statisticLabel,
	statisticValue
} from 'cs'

const { Text, Title, Paragraph } = Typography
const { Link } = Anchor

const offerData = {
	name: 'Рога и Копыта',
	money: 10000,
	document: '#commercial-offer',
	rating: {
		value: 3.8,
		reviews: 17
	},
	INN: '98578102874309049019',
	contacts: {
		phone: '+7(123)456-78-90',
		mail: 'test@example.mail',
		address: 'Кремлевская 1'
	},
	comment: 'У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. У нас большой опыт в этой сфере и мы нашли способ срезать издержки. '
}

const Chat = props => {
	const { match: { params: { participantId } } } = props

	const {
		name,
		INN,
		contacts: {
			phone,
			mail,
			address
		},
		rating,
		money,
		documentLink,
		comment
	} = offerData

	return pug`
		Divider() Общая информация
		InfoRow(label='Имя подрядчика')
			Text ${ name}
		InfoRow(label='ИНН')
			Text ${ INN }

		if (offerData.contacts)
			Divider Контактные данные
			if (phone)
				InfoRow(label='Телефон')
					Text ${ phone }
			if (mail)
				InfoRow(label='Почта' info=mail)
					Text ${ mail }
			if (address)
				InfoRow(label='Адрес' info=address)
					Text ${ address }

		Divider Детали предложения
		InfoRow(label='Стоимость')
			Text ${ money }₽
		InfoRow(label='Документ коммерческого предложения')
			Link(href=documentLink) Скачать
		InfoRow(label='Комментарий подрядчика')
			Text ${ comment }
	`
}

export default withRouter(Chat)
