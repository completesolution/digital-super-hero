import { withRouter, Link, Route } from 'react-router-dom'
import List from './scenes/List'
import Chat from './scenes/Chat'
import React from 'react'

const Participants = props => {
	const { match: { url } } = props

	return pug`
		Route(exact path=${`/competition/:competitionId/participants`} render=List)
		Route(path=${`/competition/:competitionId/participants/:participantId`} render=Chat)
	`
}

export default withRouter(Participants)
