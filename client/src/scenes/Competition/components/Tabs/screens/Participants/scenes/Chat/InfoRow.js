import React from 'react'

import { Row, Col, Typography} from 'antd'
const { Text } = Typography

export default (props) => {
	const { label, info, children } = props
	return pug`
		Row(type='flex' style={ marginBottom: '12px' })
			Col(span=1)
			Col(span=8)
				Text ${ label }
			Col(span=15)
				${ children }
	`
}
