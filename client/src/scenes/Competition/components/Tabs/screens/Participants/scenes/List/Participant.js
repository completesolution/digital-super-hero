import React from 'react'
import { withRouter, Link } from 'react-router-dom'

import {
	Layout, List, Statistic,
	Icon, Row, Col,
	Rate, Avatar, Typography
} from 'antd'

import {
	defaultListItemStyle,
	statisticLabel,
	statisticValue
} from 'cs'

const ListItem = List.Item
const { Meta } = ListItem
const { Text } = Typography

const Participant = props => {
	const {
		index,
		profileId,
		name,
		desc,
		rating,
		avatar,
		offer: {
			comment,
			money,
			term
		},
		match: {
			params: {
				competitionId
			}
		}
	} = props

	const roundedRating = Math.round(rating.value * 2) / 2;

	return pug`
		Link(to=${`/competition/${competitionId}/participants/${index}`})
			ListItem(
				style=${defaultListItemStyle()}
				extra=${pug`
						Row(style={width: '30vw', height: '100%', display: 'flex', alignItems: 'center'})
							Col(span=12 style={ textAlign: 'center'})
								Rate(value=roundedRating disabled allowHalf)
								Text(style=statisticLabel()) Рейтинг: ${rating.value}
								Text(style=statisticLabel()) (${rating.reviews} Оценок)
							Col(span=12)
								Statistic(
									title=${pug`span(style=statisticLabel()) Стоимость`}
									value=money
									suffix='₽'
									valueStyle=statisticValue()
								)
				`}
			)
				Meta(
					avatar=${pug`
						Avatar(
							src=avatar
							style={ backgroundColor: '#87d068' }
							icon='user'
						)
					`}
					title=name
					description=comment
				)
	`
}

export default withRouter(Participant)
