import { withRouter } from 'react-router-dom'
import React from 'react';

import { List } from 'antd'
import Participant from './Participant'

const data = [
	{
		index: 0,
		profileId: 357,
		name: 'Фрилансер Вася',
		rating: {
			value: 3.5,
			reviews: 17
		},
		desc: 'Я просто фрилансер Васёк.',
		offer: {
			money: 45000,
			term: 20,
			comment: 'Хорошая мысль'
		}
	},
	{
		index: 1,
		profileId: 8862,
		name: 'Компания Пылающий Легион',
		rating: {
			value: 3.8,
			reviews: 23
		},
		desc: 'Мы молодая компания, делаем много хороших вещей и вообще мы молодцы так держать. Скоро все в Азероте будут знать о наших работах, до тех пор мы будем прикладывать 300% усилий для достижения наших целей.',
		offer: {
			money: 100000,
			term: 10,
			comment: 'У нас большой опыт работы в этой сфере'
		}
	}
].map((value, index) => ({
	...value,
	id: index
}))


const renderParticipant = props => pug`
	Participant(...props)
`

const ParticipantList = props => {
	return pug`
		List(
			itemLayout='vertical'
			dataSource=data
			renderItem=renderParticipant
		)
	`
}

export default withRouter(ParticipantList)
