import React from 'react'
import { withRouter } from 'react-router-dom'
import { Row, Col, Icon, Card, Button, Upload } from 'antd'
import { Form as AntForm } from 'antd'

import {
	Form,
	TextAreaField,
	SubmitButton,
	NumberField
} from 'form'

const { Item } = AntForm

const validate = values => {
	const errors = {}
	
	return errors
}

const onSubmit = values => {
	console.log(values);
}

const onChangeUpload = ({ file, fileList }) => {
	
}

export default props => {
	return pug`
		Form(
			onSubmit=onSubmit
			validate=validate
		)
			TextAreaField(
				name='comments'
				placeholder='Комментарий...'
				autosize=true
			)
			NumberField(
				style={ width: '100%' }
				size='large'
				placeholder='Предлагаемая сумма...'
				name='cash'
			)
			Item
				Upload(
					name='offer'
					action='#url'
					onChange=onChangeUpload
				)
					Button(type='upload' icon='upload')
						|Прикрепить предложение
			SubmitButton
	`
}