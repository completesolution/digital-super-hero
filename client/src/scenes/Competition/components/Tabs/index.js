import React from 'react'
import { withRouter } from 'react-router-dom'
import { Route } from 'react-router'
import { Tabs, Button, Modal, Form } from 'antd'

import Review from './screens/Review'
import Participants from './screens/Participants'
import Docs from './screens/Docs'
import Timeline from './screens/Timeline'
import Offer from './Offer'

const { TabPane } = Tabs
const { Group: ButtonGroup } = Button
const { Item } = Form

const tabBarExtraContent = onClick => pug`
	Form(layout='inline')
		Item
			Button(onClick=onClick type='primary') Сделать предложение
		Item
			Button(href='#link' type='primary' icon='cloud-download')
				|Скачать шаблон
`

class TabMenu extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			modalVisible: false
		}
		
		this.handleOpenModal = this.handleOpenModal.bind(this)
		this.handleCloseModal = this.handleCloseModal.bind(this)
	}
	
	handleOpenModal() {
		this.setState({ modalVisible: true })
	}
	
	handleCloseModal() {
		this.setState({ modalVisible: false })
	}
	
	render() {
		const {
			match: { url, params: { id } },
			onChangeTab 
		} = this.props
		
		return pug`
			Tabs(
				onChange=onChangeTab
				tabBarExtraContent=tabBarExtraContent(this.handleOpenModal)
			)
				TabPane(tab='Обзор' key=${`${url}/review`})
					Route(
						path=${`${url}/review`}
						render=${() => pug`Review(id=id)`}
					)
				TabPane(tab='Участники' key=${`${url}/participants`})
					Route(
						path=${`${url}/participants`}
						render=${() => pug`Participants(id=id)`}
					)
				TabPane(tab='Документы' key=${`${url}/docs`})
					Route(
						path=${`${url}/docs`}
						render=${() => pug`Docs(id=id)`}
					)
				TabPane(tab='Этапы' key=${`${url}/timeline`})
					Route(
						path=${`${url}/timeline`}
						render=${() => pug`Timeline(id=id)`}
					)
			Modal(
				title='Коммерческое предложение'
				visible=this.state.modalVisible
				footer=null
				onCancel=this.handleCloseModal
			)
				Offer
		`
	}
}
// const TabMenu = props => {
// 	const { match: { url, params: { id } } } = props
// 
// 	return pug`
// 		Tabs(onChange=props.onChangeTab tabBarExtraContent=)
// 			TabPane(tab='Обзор' key=${`${url}/review`})
// 				Route(
// 					path=${`${url}/review`}
// 					render=${() => pug`Review(id=id)`}
// 				)
// 			TabPane(tab='Участники' key=${`${url}/participants`})
// 				Route(
// 					path=${`${url}/participants`}
// 					render=${() => pug`Participants(id=id)`}
// 				)
// 			TabPane(tab='Документы' key=${`${url}/docs`})
// 				Route(
// 					path=${`${url}/docs`}
// 					render=${() => pug`Docs(id=id)`}
// 				)
// 			TabPane(tab='Этапы' key=${`${url}/timeline`})
// 				Route(
// 					path=${`${url}/timeline`}
// 					render=${() => pug`Timeline(id=id)`}
// 				)
// 	`
// }

export default withRouter(TabMenu)