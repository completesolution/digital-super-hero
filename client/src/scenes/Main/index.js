import { withRouter, Link } from 'react-router-dom'
import React from 'react'
import {
	defaultListItemStyle,
	statisticLabel,
	statisticValue
} from 'cs'
import Countdown from 'Countdown'//shared
import { tagByType } from 'tagSelector'//shared
import {
	Layout, List, Statistic,
	Icon, Row, Col
} from 'antd'
import { graphql } from 'react-apollo'
import { getCompetitions } from 'queries'
import moment from 'moment'

const { Item } = List
const { Meta } = Item

const renderItems = props => {
	const {
	   id,
	   title,
	   description,
	   maxValue,
	   count = 0,
	   typeCompetition,
	   timeLimit
	} = props

	return pug`
		Link(to=${`/competition/${id}`})
			Item(
				style=${defaultListItemStyle()}
				actions=${[tagByType(typeCompetition)]}
				extra=${pug`
						Row(style={width: '30vw', height: '100%', display: 'flex', alignItems: 'center'})
							Col(span=8)
								Statistic(
									title=${pug`span(style=statisticLabel()) Стоимость`}
									value=maxValue
									suffix='₽'
									valueStyle=statisticValue()
								)
							Col(span=8)
								Statistic(
									title=${pug`span(style=statisticLabel()) Участники`}
									value=count prefix=${pug`Icon(type='team')`}
									valueStyle=statisticValue()
								)
							Col(span=8)
								Countdown(time=${moment(timeLimit).unix() * 1000})
				`}
			)
				Meta(
					title=title
					description=description
				)
	`
}

class Main extends React.Component {
	constructor(props) {
		super(props)

	}

	render() {
		const { data: { loading, competitions } } = this.props

		return pug`
			if loading
				span Загрузка...
			else
				List(
					itemLayout='vertical'
					dataSource=competitions
					renderItem=renderItems
				)
		`
	}
}

export default withRouter(graphql(getCompetitions, {
	options: {
		pollInterval: 500
	}
})(Main))
