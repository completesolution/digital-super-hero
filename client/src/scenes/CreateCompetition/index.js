import { withRouter } from 'react-router-dom'
import React from 'react'
import { me, createCompetition } from 'queries'
import { graphql, compose } from 'react-apollo'
import moment from 'moment'

import { Select } from 'antd'

const { Option } = Select

import {
   Form,
   TextField,
   TextAreaField,
   DateField,
   NumberField,
   SubmitButton,
	SelectField
} from 'form'

const CreateCompetition = props => {

	const {
		user: { loading, me },
		createCompetition
	} = props
	const { userRole, id } = me || {}

	const handleSubmit = async values => {
		values.owner = id
		values.publishedAt = moment().format()

		await createCompetition({
			variables: values
		}).then(data => console.log(data))
	}

	return pug`
		if loading
			span Загрузка...
		else if (userRole == 'CUSTOMER')
			Form(onSubmit=handleSubmit)
				SelectField(name='typeCompetition' defaultValue='COMPETITION')
					Option(key='COMPETITION' value='COMPETITION') Конкурс
					Option(key='AUCTION' value='AUCTION') Аукцион
					Option(key='TENDER' value='TENDER') Тендер
				TextField(name='title' placeholder='Название конкурса')
				TextAreaField(name='description' placeholder='Описание конкурса')
				DateField(name='timeLimit' placeholder='Дата завершения')
				NumberField(name='maxValue' placeholder='Цена')
				SubmitButton(type='submit' value='Создать')
		else
			span Вы не авторизованы
	`
}

export default withRouter(compose(
	graphql(me, { name: 'user' }),
	graphql(createCompetition, { name: 'createCompetition' })
)(CreateCompetition))
